
	<?php
	use yii\helpers\Html;
	use yii\widgets\ActiveForm;
	?>
	<?php
		$this->title = "Cek Pajak";
		$this->params['breadcrumbs'][] = ['label' => 'Cek Pajak'];
	?>
	<?php $form = ActiveForm::begin(); ?>
	<div class="col-md-12">
		<div class="col-md-4 'btn btn-material-bluegrey waves-effect waves-light'">
			<?= $form->field($model, 'nop')->textInput()->input('nop', ['placeholder' => "Masukkan NOP"]);  ?>

	    	<?= $form->field($model, 'tahun')->textInput()->input('tahun', ['placeholder' => "Masukkan Tahun Pajak"]); ?>
	    	<div class="form-group">
	        <?= Html::submitButton('Submit', ['class' => 'btn btn-primary waves-effect waves-light']) ?>
	    	</div>
		</div>
		<div class="col-md-4">
			<h1>Petunjuk</h1>
			<p>1. Silahkan Masukkan Nomor Pajak Anda</p>
			<p>2. Silahkan Masukkan Tahun Pajak Anda</p>
			<p>3. Tekan Submit</p>
		</div>
	</div>
	<?php ActiveForm::end(); ?>

